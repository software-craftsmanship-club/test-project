The project has been dockerized for convience. If you have docker installed, build the tests using
the following:
```
docker build -t fizzbuzz-tests .
```

Then run the tests using the command:
```
docker run fizzbuzz-tests
```